# perun.connector

Library for high volume machine-to-machine communication with Perun IAM system.

## Installation

Use pip:

```sh
pip install perun.connector
```

## Configuration

To use AdaptersManager, fill `adapters_manager_cfg.yaml`.

To use RPC connector or LDAP connector only, fill `openapi_cfg.yaml` or `ldap_connector_cfg.yaml` respectivelly.
